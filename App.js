import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, _ScrollView} from 'react-native';

export default function App() {
  return (
 
  <View style={styles.container}>
      <Text style={styles.headline}>Get started</Text>
      <Text style={styles.sub}>Experience the happiness</Text>
       <TextInput caretHidden={true} keyboardType={'phone-pad'} placeholder="+91 | Mobile Number" style={styles.input}  />
       

              <TouchableOpacity
                        style={styles.forButton}
                        activeOpacity = { .5 }>
                
                          <Text style={{color:'#fff',paddingLeft:'40%'}}> Continue </Text>
                            
              </TouchableOpacity>
                    
  
      <StatusBar style="auto" />
    </View>
    
  );
}

const styles = StyleSheet.create({
  headline: {
    width:'100%',
    marginLeft:25,
    textAlign: 'left', 
    fontSize: 22,
    
    
  },
  sub:{
    width:'100%',
    marginLeft:25,
    textAlign: 'left',
    paddingTop:10,
    paddingBottom:15,
    color:'grey',
   
    
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input:{
    marginBottom:'120%',
    width:380,
    height:40,
    marginTop:20,
    borderColor:'#EAEEF2', 
    backgroundColor:'#EBEEF0',
    borderRadius:5,
    borderWidth:1},
  forButton: {
    paddingTop:15,
    paddingBottom:15,
    width:'90%',
    backgroundColor:'#000000',
    borderRadius:5
}
});